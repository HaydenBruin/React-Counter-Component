import React, { Component } from 'react'

export default class Counter extends Component {
    constructor(props) {
        super(props);

        this.count = props.count;
    }

    buttonUp() {
        this.count = this.count + 1;
        this.forceUpdate();
    }
    buttonDown() {
        this.count = this.count - 1;
        this.forceUpdate();
    }

    render() {
        return (
            <div>
                <h1>Counter: {this.count}</h1>

                <button onClick={() => this.buttonUp()}>+1</button>
                <button onClick={() => this.buttonDown()}>-1</button>
            </div>
        )
    }
}
